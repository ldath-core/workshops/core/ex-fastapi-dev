from fastapi.testclient import TestClient

import os
import sys

sys.path.append(os.path.dirname(__file__) + '/../../app')


from api import app

client = TestClient(app)


def test_read_version():
    response = client.get("/version")
    assert response.status_code == 200
    assert response.json() == {"version": "0.0.1-alpha"}


def test_read_hello_msg():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello my name is Backend"}
