
*** Setting ***
Library           Process
Documentation     # Setting metadata for test suite directory
Suite Setup       Setup Backend     #  Setup of test suite directory
Suite Teardown    My Teardown  #  Teardown of test suite directory

*** Keyword ***
Setup Backend
    ${result} =     Start Process     coverage      run     --data-file     ${CURDIR}/../../.coverage     -m      uvicorn     --app-dir       ${CURDIR}../../     --host        127.0.0.1       --port     8000        app.api:app
    Log     ${result}
    Sleep  1s

My Teardown
    Terminate All Processes    kill=false
