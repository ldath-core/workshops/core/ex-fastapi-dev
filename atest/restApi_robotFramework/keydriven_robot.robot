
*** Settings ***
Documentation     Example test cases using the keyword-driven testing approach.
...
...               All tests contain a workflow constructed from keywords in
...               ``FastApi.py``. Creating new tests or editing
...               existing is easy even for people without programming skills.
...
...               The _keyword-driven_ appoach works well for normal test
...               automation, but the _gherkin_ style might be even better
...               if also business people need to understand tests. If the
...               same workflow needs to repeated multiple times, it is best
...               to use to the _data-driven_ approach.
Library           REST    http://127.0.0.1:8000


*** Test Cases ***
GET welcome message
    GET         /                  # this creates a new instance
    Output schema   response body
    Object      response body             # values are fully optional
    String      response body message     "Hello my name is Backend"
    [Teardown]  Output schema             # note the updated response schema

*** Test Cases ***
GET version
    GET         /version                  # this creates a new instance
    Output schema   response body
    Object      response body             # values are fully optional
    String      response body version     "0.0.1-alpha"
    [Teardown]  Output schema             # note the updated response schema