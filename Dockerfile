FROM python:3.9

RUN useradd -ms /bin/bash  api

USER api

WORKDIR /home/api

COPY ./requirements.txt /home/api/requirements.txt

RUN python -m venv venv

RUN /home/api/venv/bin/python -m pip install --upgrade pip

RUN /home/api/venv/bin/python -m pip install --no-cache-dir --upgrade -r /home/api/requirements.txt

COPY ./app /home/api/app

COPY main.py /home/api/

CMD ["/home/api/venv/bin/python", "/home/api/main.py"]

