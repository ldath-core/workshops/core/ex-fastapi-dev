# Fast API sample

Sample usage of fastAPI python packages, to create simple API server listening on port 8000.


## Building and running docker

To start docker run:

```sh

docker build -t fast_api .
docker run -d --network="host" fast_api

```

## Running test

Example test frameworks with coverage that implement sample tests for API:


### RobotFramework

About the tool: [link](https://robotframework.org/)

```sh

python -m venv venv;
source ./venv/bin/activate
pip install -r ./requirements/apitest_robot.txt
robot --outputdir results atest/restApi_robotFramework
coverage html
```

### PyTest

About testing logic: [link](https://fastapi.tiangolo.com/tutorial/testing/)

```sh

python -m venv venv;
source ./venv/bin/activate
pip install -r ./requirements/apitest_pytest.txt
pytest --cov-report html  --cov=api atest/restApi_pytest --junitxml=pytest_results/results.xml
```
